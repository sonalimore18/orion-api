<?php

namespace App\Containers\Company\Models;
use App\Containers\User\Models\User;
use App\Ship\Parents\Models\Model;

class Company extends Model
{
    protected $fillable = [
      "name","created_by","modified_by","status"
    ];

    protected $attributes = [

    ];

    protected $hidden = [

    ];

    protected $casts = [

    ];

    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * A resource key to be used by the the JSON API Serializer responses.
     */
    protected $resourceKey = 'companies';

    public function created_by_user() {
      return $this->belongsTo(User::class, 'created_by');
    }

    public function modified_by_user() {
      return $this->belongsTo(User::class, 'modified_by');
    }
}
