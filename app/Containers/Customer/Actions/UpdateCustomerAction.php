<?php

namespace App\Containers\Customer\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use DB;

class UpdateCustomerAction extends Action
{
    public function run(Request $request)
    {
        try {
          DB::beginTransaction();

          $customer = Apiato::call('Customer@UpdateCustomerTask', [$request->id, $request->all()]);

          DB::commit();
          return $customer;
        } catch (\Exception $ex) {
          DB::rollback();
          throw new \Exception($ex->getMessage(), $ex->getCode());
        }
    }
}
