<?php

namespace App\Containers\Company\UI\API\Transformers;

use App\Containers\Company\Models\Company;
use App\Ship\Parents\Transformers\Transformer;
use App\Containers\User\UI\API\Transformers\CreatedByUserTransformer;
use App\Containers\User\UI\API\Transformers\ModifiedByUserTransformer;

class CompanyTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [
      'created_by_user','modified_by_user'
    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [

    ];

    /**
     * @param Company $entity
     *
     * @return array
     */
    public function transform(Company $entity)
    {
        $response = [
            'object' => 'Company',
            'id' => $entity->getHashedKey(),
            'name' => $entity->name,
            'status' => $entity->status,
            'created_at' => date('Y-m-d h:i:s', strtotime($entity->created_at)),
            'updated_at' => date('Y-m-d h:i:s', strtotime($entity->updated_at))
        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }

    /**
     * get relational created by user details
     */
    public function includeCreatedByUser(Company $item){
      if(isset($item->created_by_user) && !empty($item->created_by_user)){
        return $this->item($item->created_by_user, new CreatedByUserTransformer());
      }
    }

    /**
     * get relational modified by user details
     */
    public function includeModifiedByUser(Company $item){
      if(isset($item->modified_by_user) && !empty($item->modified_by_user)){
        return $this->item($item->modified_by_user, new ModifiedByUserTransformer());
      }
    }
}
