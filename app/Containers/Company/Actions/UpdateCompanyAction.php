<?php

namespace App\Containers\Company\Actions;

use App\Ship\Parents\Actions\Action;
use App\Ship\Parents\Requests\Request;
use Apiato\Core\Foundation\Facades\Apiato;
use DB;

class UpdateCompanyAction extends Action
{
    public function run(Request $request)
    {
        try {
          DB::beginTransaction();

          $company = Apiato::call('Company@UpdateCompanyTask', [$request->id, $request->all()]);

          DB::commit();
          return $company;
        } catch (\Exception $ex) {
          DB::rollback();
          throw new \Exception($ex->getMessage(), $ex->getCode());
        }
    }
}
