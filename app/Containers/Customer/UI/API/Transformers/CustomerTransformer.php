<?php

namespace App\Containers\Customer\UI\API\Transformers;

use App\Containers\Customer\Models\Customer;
use App\Ship\Parents\Transformers\Transformer;
use App\Containers\User\UI\API\Transformers\CreatedByUserTransformer;
use App\Containers\User\UI\API\Transformers\ModifiedByUserTransformer;

class CustomerTransformer extends Transformer
{
    /**
     * @var  array
     */
    protected $defaultIncludes = [

    ];

    /**
     * @var  array
     */
    protected $availableIncludes = [
      'created_by_user','modified_by_user'
    ];

    /**
     * @param Customer $entity
     *
     * @return array
     */
    public function transform(Customer $entity)
    {
        $response = [
            'object' => 'Customer',
            'id' => $entity->getHashedKey(),
            'company_name' => $entity->company_name,
            'address' => $entity->address,
            'logo' => $entity->logo,
            'max_site' => $entity->max_site,
            'status' => $entity->status,
            'created_at' => date('Y-m-d h:i:s', strtotime($entity->created_at)),
            'updated_at' => date('Y-m-d h:i:s', strtotime($entity->updated_at))
        ];

        $response = $this->ifAdmin([
            'real_id'    => $entity->id,
            // 'deleted_at' => $entity->deleted_at,
        ], $response);

        return $response;
    }

    /**
     * get relational created by user details
     */
    public function includeCreatedByUser(Customer $item){
      if(isset($item->created_by_user) && !empty($item->created_by_user)){
        return $this->item($item->created_by_user, new CreatedByUserTransformer());
      }
    }

    /**
     * get relational modified by user details
     */
    public function includeModifiedByUser(Customer $item){
      if(isset($item->modified_by_user) && !empty($item->modified_by_user)){
        return $this->item($item->modified_by_user, new ModifiedByUserTransformer());
      }
    }
}
