<?php

namespace App\Containers\User\UI\API\Transformers;

use App\Containers\User\Models\User;
use App\Ship\Parents\Transformers\Transformer;

class CreatedByUserTransformer extends Transformer
{
    /**
     * @param User $entity
     *
     * @return array
     */
    public function transform(User $entity)
    {
        return [
            'id' => $entity->getHashedKey(),
            'name' => isset($entity->name)? $entity->name: null
        ];
    }

}
